**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
# react-native-tap-payment
React Native Tap Payment binding for Android platform

#### Support
- Android: version should be <= 0.59
- iOS: no support

## Getting started

`$ npm install react-native-tap-payment --save`

### Mostly automatic installation

`$ react-native link react-native-tap-payment`

### Manual installation

#### iOS

We are developing...
<!-- 1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-tap-payment` and add `RNTapPayment.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNTapPayment.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)< -->

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.tappayment.RNTapPaymentPackage;` to the imports at the top of the file
  - Add `new RNTapPaymentPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-tap-payment'
  	project(':react-native-tap-payment').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-tap-payment/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-tap-payment')
  	```


## Usage
```javascript
import TapPayment from 'react-native-tap-payment';

let data = {
	SecretAPIkey: 'sk_test_kovrMB0mupFJXfNZWx6Etg5y',
	AppID: "company.tap.goSellSDKExample",
	CustomerId: '', //if customer id available then CustomerId :'cus_c1R02820192008h1X10805371', else empty string
	price: 199,
	Currency: 'KWD',
	UILanguage: 'en',
	Customer: {
		firstName: 'Barack',
		lastName: 'Obama',
		email: 'barack@gmail.com',
		countryCode: '965',
		phoneNumber: '1234567'
	}
}


TapPayment.openTapPaymentUI(data, (result) => {
	console.log('payment result', result)
})

```
